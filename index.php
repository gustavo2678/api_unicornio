<?php

use Phalcon\Loader;
use Phalcon\Mvc\Micro;
use Phalcon\Di\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as PdoMysql;
use Phalcon\Http\Response;

// Use Loader() to autoload our model
$loader = new Loader();

$loader->registerNamespaces(
    [
        'Store\Toys' => __DIR__ . '/models/',
    ]
);

$loader->register();

$di = new FactoryDefault();

// Set up the database service
$di->set(
    'db',
    function () {
        return new PdoMysql(
            [
                'host'     => 'localhost',
                'username' => 'root',
                'password' => '',
                'dbname'   => 'rentger_unicornio',
            ]
        );
    }
);

// Create and bind the DI to the application
$app = new Micro($di);

// Retrieves all Unicornios
$app->get(
    '/api/unicornio',
    function () use ($app){        
      $phql = 'SELECT * FROM Store\Toys\Unicornio ORDER BY Id';
            
      $unicornios = $app->modelsManager->executeQuery($phql);      

      $data = [];

     foreach ($unicornios as $unicornio) {         
         $data[] = [
             'Id'   => $unicornio->Id,
             'Nombre' => $unicornio->Nombre,
             'Color'   => $unicornio->Color,
             'Precio' => $unicornio->Precio,
         ];
     }
     
     echo json_encode($data);     
    }
);

// Searches for Unicornios with $name in their name
$app->get(
    '/api/unicornio/{name}',
    function ($name) use ($app) {
      $phql = 'SELECT * FROM Store\Toys\Unicornio WHERE Nombre LIKE :name: ORDER BY Nombre';

        $unicornios = $app->modelsManager->executeQuery(
            $phql,
            [
                'name' => '%' . $name . '%'
            ]
        );
        
        $data = [];

        foreach ($unicornios as $unicornio) {           
            $data[] = [
              'Id'   => $unicornio->Id,
              'Nombre' => $unicornio->Nombre,
              'Color'   => $unicornio->Color,
              'Precio' => $unicornio->Precio,
            ];
        }

        echo json_encode($data);
    }
);

// Retrieves Unicornios based on primary key
$app->get(
    '/api/unicornio/{id:[0-9]+}',
    function ($id) use ($app) {
        $phql = 'SELECT * FROM Store\Toys\Unicornio WHERE Id = :Id:';

        $unicornios = $app->modelsManager->executeQuery(
            $phql,
            [
                'Id' => $id,
            ]
        );

        $data = [];

        foreach ($unicornios as $unicornio) {
            $data[] = [
              'Id'   => $unicornio->Id,
              'Nombre' => $unicornio->Nombre,
              'Color'   => $unicornio->Color,
              'Precio' => $unicornio->Precio,
            ];
        }

        echo json_encode($data); 
    }
);

$app->notFound(function () use ($app) {
  $app->response->setStatusCode(404, "Not Found")->sendHeaders();
  echo 'No se ha encontrado la pagina!'; });

$app->handle();
